#!/bin/bash
#title           :lblogs.bash
#description     :This script gets load balancer logs from Rackspace
#                 Concatenates them into one log file
#                 Updates awstats with data from log file
#                 Rotates log file if required
#
#author          :Aaron Coach
#notes           :

accountid="000000"
username="user.name"
apikey="apikey"
#this_month_year=$(date +%b_%Y)

http_log_container="lb_42255_Prod-http-LB-01_"
log_location="/var/log/loadbalancer/"

function authrequest
{
    response=$(curl -sSd \
    "{
        \"auth\":
        {
            \"RAX-KSKEY:apiKeyCredentials\":
            {
                \"username\": \"$username\",
                \"apiKey\": \"$apikey\"
            }
        }
    }" \
    -H 'Content-Type: application/json' \
    'https://identity.api.rackspacecloud.com/v2.0/tokens')

    authid=$(echo $response | jq --raw-output ".access.token.id")
    publicurl=$(echo $response | jq --raw-output ".access.serviceCatalog[1].endpoints[0].publicURL")
}

authrequest

# Download files and merge into loadbalancer.log file
# Remove files after
for i in $(seq 0 1)
do
    month_year=$(date +%b_%Y --date "${i} month ago")
    for i in $(curl -s -H "X-Auth-Token: $authid" -X GET ${publicurl}/${http_log_container}${month_year}?prefix=lb_)
    do
        FILE=$(echo "$i" | cut -d '/' -f6);
        curl -sSH "X-Auth-Token: $authid" -X GET ${publicurl}/${http_log_container}${month_year}/$FILE -o $FILE;
        if test -r $FILE
        then
            zcat $FILE >> ${log_location}loadbalancer.log;
            rm -f $FILE;
            curl -sSH "X-Auth-Token: $authid" -X DELETE ${publicurl}/${http_log_container}${month_year}/$FILE;
        else
            echo "Something went wrong with the log download."
            exit 22
        fi
    done
done

# Update awstats
for i in $(ls -I awstats.model.conf /etc/awstats)
do
    tmp=${i##awstats.}
    /usr/bin/perl /usr/local/awstats/wwwroot/cgi-bin/awstats.pl -config=${tmp%%.conf} -update
done

# Rotate logs
/usr/sbin/logrotate /root/scripts/lbrotate
